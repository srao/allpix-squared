---
template: overrides/main.html
title: "Objects"
---

Object Types 
------------

Allpix² provides a set of objects which can be used to transfer data
between modules. These objects can be sent with the messaging system as
explained in Section [Passing Objects using Messages](framework-passing-objects-using-messages.md). A `typedef` is added to
every object in order to provide an alternative name for the message
which is directly indicating the carried object.

The list of currently supported objects comprises:

##### MCTrack

The MCTrack objects reflects the state of a particle’s trajectory when
it was created and when it terminates. Moreover, it allows to retrieve
the hierarchy of secondary tracks. This can be done via the parent-child
relations the MCTrack objects store, allowing retrieval of the primary
track for a given track. Combining this information with MCParticles
allows the Monte-Carlo trajectory to be fully reconstructed. In addition
to these relational information, the MCTrack stores information on the
initial and final point of the trajectory (in coordinates), the energies
(total as well as kinetic only) at those points, the creation process
type, name, and the volume it took place in. Furthermore, the particle’s
PDG id is stored.

##### MCParticle

The Monte-Carlo truth information about the particle passage through the
sensor. A start and end point are stored in the object: for events
involving a single MCParticle passing through the sensor, the start and
end points correspond to the entry and exit points. The exact handling
of non-linear particle trajectories due to multiple scattering is up to
module. In addition, it provides a member function to retrieve the
reference point at the sensor center plane in local coordinates for
convenience. The MCParticle also stores an identifier of the particle
type, using the PDG particle codes[^24], as well as the time it has
first been observed in the respective sensor. The MCParticle
additionally stores a parent MCParticle object, if available. The lack
of a parent doesn’t guarantee that this MCParticle originates from a
primary particle, but only means that no parent on the given detector
exists. Also, the MCParticle stores a reference to the MCTrack it is
associated with.

##### DepositedCharge

The set of charge carriers deposited by an ionizing particle crossing
the active material of the sensor. The object stores the position in the
sensor together with the total number of deposited charges in elementary
charge units. In addition, the time (in *ns* as the internal framework
unit) of the deposition after the start of the event and the type of
carrier (electron or hole) is stored.

##### PropagatedCharge

The set of charge carriers propagated through the silicon sensor due to
drift and/or diffusion processes. The object should store the final
position of the propagated charges. This is either on the pixel implant
(if the set of charge carriers are ready to be collected) or on any
other position in the sensor if the set of charge carriers got trapped
or was lost in another process. Timing information giving the total time
to arrive at the final location, from the start of the event, can also
be stored.

##### PixelCharge

The set of charge carriers collected at a single pixel. The pixel
indices are stored in both the $x$ and $y$ direction, starting from zero
for the first pixel. Only the total number of charges at the pixel is
currently stored, the timing information of the individual charges can
be retrieved from the related `PropagatedCharge` objects.

##### PixelHit

The digitised pixel hits after processing in the detector’s front-end
electronics. The object allows the storage of both the time and signal
value. The time can be stored in an arbitrary unit used to timestamp the
hits. The signal can hold different kinds of information depending on
the type of the digitizer used. Examples of the signal information is
the ’true’ information of a binary readout chip, the number of ADC
counts or the ToT (time-over-threshold).

Object History 
--------------

Objects may carry information about the objects which were used to
create them. For example, a `PropagatedCharge` could hold a link to the
`DepositedCharge` object at which the propagation started. All objects
created during a single simulation event are accessible until the end of
the event; more information on object persistency within the framework
can be found in Chapter [Allpix SquaredPassing Objects using Messages](framework-passing-objects-using-messages.md#persistency).

Object history is implemented using the ROOT TRef class[^17], which
acts as a special reference. On construction, every object gets a unique
identifier assigned, that can be stored in other linked objects. This
identifier can be used to retrieve the history, even after the objects
are written out to ROOT TTrees[^16]. TRef objects are however not
automatically fetched and can only be retrieved if their linked objects
are available in memory, which has to be ensured explicitly. Outside the
framework this means that the relevant tree containing the linked
objects should be retrieved and loaded at the same entry as the object
that request the history. Whenever the related object is not in memory
(either because it is not available or not fetched) a
`MissingReferenceException` will be thrown.

A MCTrack which originated from another MCTrack is linked via a
reference to this track, this way the track hierarchy can be obtained.
Every MCParticle is linked to the MCTrack it is associated with. A
MCParticle can furthermore be linked to another MCParticle on the same
detector. This will be the case if there are MCParticles from a primary
(parent) and secondary (child) track on one detector. The corresponding
child MCParticles will then carry a reference to the parent MCParticle.

[^16]:Rene Brun and Fons Rademakers. ROOT User’s Guide. Trees. url: [https://root.cern.ch/root/htmldoc/guides/users-guide/Trees.html](https://root.cern.ch/root/htmldoc/guides/users-guide/Trees.html).
[^17]:Rene Brun and Fons Rademakers. ROOT User’s Guide. Input/Output. url: [https://root.cern.ch/root/htmldoc/guides/users-guide/InputOutput.html](https://root.cern.ch/root/htmldoc/guides/users-guide/InputOutput.html).
[^24]:L. Garren et al. Monte Carlo Particle Numbering Scheme. 2015. url: [http://hepdata.cedar.ac.uk/lbl/2016/reviews/rpp2016-rev-monte-carlo-numbering.pdf](http://hepdata.cedar.ac.uk/lbl/2016/reviews/rpp2016-rev-monte-carlo-numbering.pdf).