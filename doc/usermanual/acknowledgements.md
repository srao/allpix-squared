---
template: overrides/main.html
title: "Acknowledgments"
---

Allpix² has been developed and is maintained by

-   Koen Wolters, (CERN)

-   Daniel Hynds, Nikhef

-   Paul Schütze, DESY

-   Simon Spannagel, DESY

The following authors, in alphabetical order, have contributed to
Allpix²:

-   Mohamed Moanis Ali, Free University of Bozen-Bolzano

-   Mathieu Benoit, BNL

-   Thomas Billoud, Université de Montréal

-   Tobias Bisanz, CERN

-   Koen van den Brandt, Nikhef

-   Carsten Daniel Burgard, DESY

-   Liejian Chen, Institute of High Energy Physics Beijing

-   Katharina Dort, University of Gießen

-   Neal Gauvin, Université de Genève

-   Lennart Huth, DESY

-   Maoqiang Jing, University of South China, Institute of High Energy
    Physics Beijing

-   Moritz Kiehn, Université de Genève

-   Salman Maqbool, CERN Summer Student

-   Sebastien Murphy, ETHZ

-   Andreas Matthias Nürnberg, KIT

-   Sebastian Pape, TU Dortmund University

-   Marko Petric, CERN

-   Nashad Rahman, The Ohio State University

-   Edoardo Rossi, DESY

-   Andre Sailer, CERN

-   Enrico Jr. Schioppa, Unisalento and INFN Lecce

-   Sanchit Sharma, Kansas State University

-   Xin Shi, Institute of High Energy Physics Beijing

-   Ondrej Theiner, Charles University

-   Annika Vauth, University of Hamburg

-   Mateus Vicente Barreto Pinto, CERN

-   Andy Wharton, Lancaster University

-   Morag Williams, University of Glasgow

The authors would also like to express their thanks to the developers of
AllPix[^3][^4].

[^3]:Mathieu Benoit and John Idarraga. The AllPix Simulation Framework. Mar. 21, 2017. url: [https://twiki.cern.ch/twiki/bin/view/Main/AllPix](https://twiki.cern.ch/twiki/bin/view/Main/AllPix).
[^4]:Mathieu Benoit, John Idarraga, and Samir Arfaoui. AllPix. Generic simulation for pixel detectors. url: [https://github.com/ALLPix/allpix](https://github.com/ALLPix/allpix).